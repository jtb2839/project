#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "map.h"

#define A(i,j) a[(i)+(j)*n]

double myround(double);
void MapInit(struct Map *map, short *arr, int w, int h, int tT)
{
	map->array=arr;
	map->width=w;
	map->height=h;
	map->tilesTraversed=0;
	map->tilesObserved=0;
	map->totalTiles=tT;
}

struct Plane movePlane(struct Map* m, struct Plane p, bool t, bool left)
{
	if(t)
		turn(&p,left);
	move(&p);

	int x=(int)myround(p.x);
	int y=(int)myround(p.y);
	if(x>0 && y>0 && x<m->width && y<m->height)
	{
		if(getTile(m,x,y)!=-1)
		{
			if(getTile(m,x,y)==3)
			{
				setTile(m,x,y,5);
			}
			else if(getTile(m,x,y)==0)
			{
				setTile(m,x,y,6);
				observeTiles(m,x,y,p.height * sin(p.cameraFOV/2));
			}
			else if(getTile(m,x,y)==1 || getTile(m,x,y)==2)
			{
				observeTiles(m,x,y,p.height * sin(p.cameraFOV/2));
				setTile(m,x,y,3);
			}

		}
	}
	return p;
}
double percentObserved(struct Map* m)
{
	/*
	int i,j;
	double num=0,tot=0;
	for(j=0;j<m->height;j++)
	{
		for(i=0;i<m->width;i++)
		{
			if(m->array[i+j*m->width]==2||m->array[i+j*m->width]==3||m->array[i+j*m->width]==5)
			{
				num++;
				tot++;
			}
			else if(m->array[i+j*m->width]==1)
			{
				tot++;
			}
		}
	}
	//printf("%f, %f\n",num,tot);
	return num/tot;
	*/
	
	if(m->totalTiles<m->tilesObserved)
		printf("%d, %d\n",m->tilesObserved,m->totalTiles);
	return ((double)(m->tilesObserved))/m->totalTiles;
	
}
short getTile(struct Map* m, int x, int y)
{
	return m->array[x+y * m->width];
}
short setTile(struct Map* m, int x, int y, short s)
{
	short old=m->array[x+y * m->width];
	m->array[x + y * m->width]=s;
	return old;
}
void observeTiles(struct Map* m, int x, int y, double radius)
{
	int i,j;
	radius=4;
	for(i=x-radius;i<x+radius;i++)
	{
		for(j=y-radius;j<y+radius;j++)
		{
			if((i>0) && (j>0) && (i<m->width) && (j<m->height) &&((m->array[i+j * m->width])==1))
			{
			if((x-i)*(x-i) + (y-j)*(y-j)<radius*radius)
			{
				m->array[i + j*m->width]=2;
				m->tilesObserved++;
			}}
		}
	}
}
double myround(double d)
{
	if((int)d+.5>d)
		return (int)d;
	return (int)d+1.0;
}
void copyMap(struct Map from, struct Map* to)
{
	to->width=from.width;
	to->height=from.height;
	to->tilesObserved=from.tilesObserved;
	to->tilesTraversed=from.tilesTraversed;
	to->totalTiles=from.totalTiles;
	to->array=malloc(from.width*from.height*sizeof(short));
	int i,j;
	int width=from.width;
	for(i=0;i<width;i++)
	{
		for(j=0;j<width;j++)
		{
			to->array[i+j*width]=from.array[i+j*width];
		}
	}
}