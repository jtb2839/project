#include "plane.h"
#include <limits.h>
#include <stdio.h>
#include <math.h>
void PlaneInit(struct Plane *plane, double sx, double sy, double sh, double FOV, double vel, double sdir, double maxDel, double baseDel)
{
	plane->x=sx;
	plane->y=sy;
	plane->height=sh;
	plane->cameraFOV=FOV;
	plane->velocity=vel;
	plane->direction=sdir;
	plane->maxDelta=maxDel;
	plane->baseDelta=baseDel;
	plane->turnRate=0;
}
void move(struct Plane* p)
{
	p->direction+=p->turnRate;
	p->x+=p->velocity*cos(p->direction);
	p->y+=p->velocity*sin(p->direction);
}
void turn(struct Plane* p, bool left)
{
	if(left&&p->turnRate<p->maxDelta)
	{
		p->turnRate+=p->baseDelta;
	}
	else if(p->turnRate> (-1)* p->maxDelta)
	{
		p->turnRate-=p->baseDelta;
	}
	p->direction+=p->turnRate;
}
