/*TODO: 
 * Map: movePlane needs to observe, traverse tiles
 * Use splines, etc to get better coverage of camera
 * Mission: Accuracy variable (iterations per second) should affect velocity, time.
 * Store more than just the best map
 * Number the tiles the plane crosses to get a good ordering
 */


#include "map.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#define A(i,j) A[(i)+(j)*boundarySize]
#define pi 3.141592653589793238462
#define red "\x1b[31m"
#define green "\x1b[32m"
#define yellow "\x1b[33m"
#define hrm "\x1b[34m"
#define magenta "\x1b[35m"
#define reset "\x1b[0m"

void createMat(short*);
void displayMat(short*);
void fly(struct Map, struct Plane, int,int,int);
short *A;
int boundarySize;
int fieldSize;
double bestExplored=0;
int bestIters=10000000;
//0=boundary zone, 1=unobserved, 2=observed, 3=traversed, -1=not part of map
int main(int argc, char* argv[])
{
	boundarySize=atoi(argv[1]);
	fieldSize=atoi(argv[2]);
	short *B=malloc(boundarySize*boundarySize*sizeof(short));
	createMat(B);
	struct Map map;
	struct Plane plane;
	MapInit(&map,B,boundarySize,boundarySize,fieldSize*fieldSize);
	PlaneInit(&plane,(boundarySize-fieldSize)/2.0,(boundarySize-fieldSize)/2.0,5.0,pi/8,3.0,0,pi/4,pi/8);
	fly(map,plane,0,0,0);
	printf("%f\n",bestExplored);
	displayMat(A); //segmentation fault
}

void fly(struct Map m, struct Plane p, int consOut,int iters, int code)//consOut is the consecutive iterations inside the boundary zone
{
	if(iters>bestIters)
		return;
	//printf("%d\t%d\t",iters,code);
	double po = percentObserved(&m);
	if(po==1.0)//success
	{
		displayMat(m.array);
		A=(&m)->array;
		bestExplored=1.0;
		if(iters<bestIters)
			bestIters=iters;
		return;
	}
	if(p.x<0 || p.y<0 || p.x>m.width || p.y>m.height || getTile(&m,p.x,p.y)==-1 || getTile(&m,p.x,p.y)== 5||getTile(&m,p.x,p.y)==6|| consOut>2)//failure
	{
		if(bestExplored<po)
		{
			printf("%f, %f\n",bestExplored,po);
			bestExplored=po;
			A=(&m)->array;
		
		}
	//	if(bestExplored>.14)
	//		printf("%d\n",iters);
	//	if(iters>150000)
	//		displayMat(m.array);
		return;
	}	
	if(getTile(&m,p.x,p.y)==0||getTile(&m,p.x,p.y)==3)
		consOut++;
	else
		consOut=0;
//	printf("%d\n",iters);

	struct Map m1;
	struct Map m2;
	struct Map m3;
	copyMap(m,&m1);
	copyMap(m,&m2);
	copyMap(m,&m3);
	fly(m1,movePlane(&m1,p,true,true),consOut,iters+1,1);//turn left
	fly(m2,movePlane(&m2,p,true,false),consOut,iters+1,2);//turn right
	fly(m3,movePlane(&m3,p,false,false),consOut,iters+1,3);//fly straight
}






void displayMat(short *A)
{
	int i,j;
	for(i=0;i<boundarySize;i++)
	{
		for(j=0;j<boundarySize;j++)
		{
			if(A(i,j)==5)
				printf(magenta "%hd ",A(i,j));
			else if(A(i,j)==3)
				printf(red "%hd ",A(i,j));
			else if(A(i,j)==2)
				printf(green "%hd ", A(i,j));
			else if(A(i,j)==1)
				printf(yellow "%hd ", A(i,j));
			else if(A(i,j)==6)
				printf(hrm "%hd ",A(i,j));
			else
			printf(reset "%hd ",A(i,j));
		}
		printf("\n");
	}
	printf(reset);
}
void createMat(short *A)
{
	int i,j;
	int helper=(boundarySize-fieldSize)/2;
	for(i=0;i<boundarySize;i++)
	{
		for(j=0;j<boundarySize;j++)
		{
			if(i>helper-1 && i<boundarySize-helper && j>helper-1 && j<boundarySize-helper)
			{
				A(i,j)=1;
			}
			else
			{
				A(i,j)=0;
			}

		}
	}
}
