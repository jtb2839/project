#include <stdbool.h>
struct Plane
{
	double x;
	double y;
	double height;
	double cameraFOV;
	double direction;
	double maxDelta;
	double velocity;
	double turnRate;
	double baseDelta;
};
void move(struct Plane*);
void turn(struct Plane*, bool);
void PlaneInit(struct Plane*, double,double,double,double,double,double,double,double);
